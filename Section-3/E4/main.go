package main

import (
	"fmt"
	"sync"
)

var (
	wg sync.WaitGroup
)

type Income struct {
	Source string
	Amount int
}

func main() {

	// variable fo bank balance

	var bankBalance int
	var mutex sync.Mutex

	// print out starting values

	fmt.Printf("Initial account balance: $%d.00\n", bankBalance)

	// define weekly revenue
	incomes := []Income{
		{Source: "Main job", Amount: 500},
		{Source: "Gifts", Amount: 10},
		{Source: "Part time job", Amount: 50},
		{Source: "Investment", Amount: 100},
	}

	// loop through 52 weeks and print out how much is made; keep a running total

	wg.Add(len(incomes))
	for index, income := range incomes {

		go func(i int, income Income) {
			defer wg.Done()

			for week := 0; week < 52; week++ {

				mutex.Lock()
				temp := bankBalance
				temp += income.Amount
				bankBalance = temp
				mutex.Unlock()
				fmt.Printf("On week %d you earned %d from %v\n", week, income.Amount, income.Source)
			}

		}(index, income)
	}

	wg.Wait()
	// print put final balance
	fmt.Printf("Final bank balance: $%d.00\n", bankBalance)

}
