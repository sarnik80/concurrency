package main

import (
	"fmt"
	"sync"
)

var (
	number int
	wg     sync.WaitGroup
)

func add(m *sync.Mutex) {
	defer wg.Done()

	m.Lock()
	number++
	m.Unlock()
}

func mines(m *sync.Mutex) {
	defer wg.Done()

	m.Lock()
	number--
	m.Unlock()
}

func main() {

	number = 5

	var mutex sync.Mutex

	wg.Add(2)
	go add(&mutex)
	go mines(&mutex)
	wg.Wait()

	fmt.Println(number)

}
