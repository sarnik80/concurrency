package main

// Race Condition example

import (
	"fmt"
	"sync"
)

var (
	msg string
	wg  sync.WaitGroup
)

func updateMessage(s string, m *sync.Mutex) {
	defer wg.Done()

	// exclusive access to the msg
	m.Lock()
	msg = s
	m.Unlock()
}

func main() {

	msg = "Hello, world!"

	var mutex sync.Mutex

	wg.Add(2)

	go updateMessage("Hello, universe!", &mutex)
	go updateMessage("Hello, cosmos!", &mutex)

	wg.Wait()

	fmt.Println(msg)
}
