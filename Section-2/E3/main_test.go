package main

import (
	"io"
	"os"
	"strings"
	"sync"
	"testing"
)

// Test_printSomething tests a function that prints something on the console
func Test_printSomething(t *testing.T) {

	StdOUT := os.Stdout

	read, write, _ := os.Pipe()
	os.Stdout = write

	var wg sync.WaitGroup
	wg.Add(1)

	go printSomething("epsilon", &wg)

	wg.Wait()

	_ = write.Close()

	result, _ := io.ReadAll(read)
	output := string(result)
	os.Stdout = StdOUT

	if !strings.Contains(output, "epsilon") {
		t.Errorf("Expected to find epsilon, but find :%s", output)
	}

}
