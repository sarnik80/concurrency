package main

import (
	"io"
	"os"
	"strings"
	"testing"
)

func Test_updateMessage(t *testing.T) {

	wg.Add(1)
	go updateMessage("epsilon")
	wg.Wait()

	if msg != "epsilon" {
		t.Errorf("Expected epsilon but it is %s", msg)
	}

}

func Test_printMessage(t *testing.T) {

	// it is same as test in E3

	StdOUT := os.Stdout

	read, write, _ := os.Pipe()
	os.Stdout = write

	msg = "epsilon"

	printMessage()

	_ = write.Close()

	result, _ := io.ReadAll(read)
	output := string(result)
	os.Stdout = StdOUT

	if !strings.Contains(output, "epsilon") {
		t.Errorf("Expected to find epsilon, but find :%s", output)
	}

}

func Test_main(t *testing.T) {

	StdOUT := os.Stdout

	read, write, _ := os.Pipe()
	os.Stdout = write

	main()

	_ = write.Close()

	result, _ := io.ReadAll(read)
	output := string(result)
	os.Stdout = StdOUT

	if !strings.Contains(output, "Hello, universe!") {
		t.Errorf("Expected to find epsilon, but find :%s", output)
	}

	if !strings.Contains(output, "Hello, cosmos!") {
		t.Errorf("Expected to find epsilon, but find :%s", output)
	}

	if !strings.Contains(output, "Hello, world!") {
		t.Errorf("Expected to find epsilon, but find :%s", output)
	}

}
