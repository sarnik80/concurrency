package main

import (
	"fmt"
	"sync"
)

var (
	msg string
	wg  sync.WaitGroup
)

func updateMessage(s string) {
	defer wg.Done()
	msg = s
}

func printMessage() {
	fmt.Println(msg)
}

func main() {

	msg = "Hello, world!"

	sentences := []string{
		"Hello, universe!",
		"Hello, cosmos!",
		"Hello, world!",
	}

	for i := 0; i < 3; i++ {
		wg.Add(1)
		go updateMessage(sentences[i])
		wg.Wait()
		printMessage()

	}

}
